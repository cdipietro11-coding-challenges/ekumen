#encoding: utf-8

import yaml
import math
import argparse as ap


def polygon_nbr_vertex(value):
    ivalue = int(value)
    if ivalue < 3:
        raise ap.ArgumentTypeError("%s is an invalid number of vertices. Polygons have at least 3 vertices." % value)
    return ivalue


def turtlesim_radius(value):
    fvalue = float(value)
    if fvalue < 0.5 or fvalue > 5.5:
        raise ap.ArgumentTypeError("%s is an invalid radius. It exceeds ROS turtlesim dimensions." % value)
    return fvalue


def rotate(point, angle):
    """
    Rotating point (s,t)  about the origin clockwise 
    through an angle θ ends up at (u,v) where:
        u = s.cos(θ)+t.sin(θ)
        v = -s.sin(θ)+t.cos(θ)
    """

    x,y = point
    angle = angle
    u = x * math.cos(angle) + y * math.sin(angle)
    v = -x * math.sin(angle) + y * math.cos(angle)
    return [u,v]


def get_rotated_points(initial_point, rotation_angles):
    rotation = lambda angle: rotate(initial_point, angle)
    return map(rotation, rotation_angles) 


def save_output(outfile, points):
    data = dict(vertices = points, nbr_turtles = len(points))
    with open(outfile, 'w') as out:
        out.write( yaml.dump(data, default_flow_style=False) )
    print 'Generated ouput was written in {0}'.format(outfile)
