#!/usr/bin/env python
#encoding: utf-8

import sys
import math
import argparse as ap
from utils import *


def parse_arguments():
    """
    Parses node arguments.
    """
    parser = ap.ArgumentParser()
    parser.add_argument('-r', '--radius', default=3.0, type=turtlesim_radius, help='radius of the smalles circle containing the wanted poylgon.')
    parser.add_argument('-v', '--nbr_vertices', default=3, type=polygon_nbr_vertex, help='number of vertices of the wanted polygon.')
    parser.add_argument('-o', '--output', type=str,  help='Output file')
    return parser.parse_args()


def get_theta(radius, nbr_vertices):
    """
    Given two point A and B in a circle of center O, 
    the arc AB defines an angle theta comprehended 
    between the vectos OA and OB. Then:
        arc_length = theta*radius
    """

    cicle_length = 2 * math.pi * radius
    arc_length = cicle_length / float(nbr_vertices)
    theta = arc_length / float(radius)
    return theta


def get_rotation_angles(radius, nbr_vertices):
    theta = get_theta(radius, nbr_vertices)
    return map(lambda i: i*theta, range(nbr_vertices))


def main(*args):
    """
    Main algorithm.
    """

    # Get arguments
    args = parse_arguments()

    # Intial point: (r, 0)
    initial_point = (args.radius, 0)

    # Rotation angles
    rotation_angles = get_rotation_angles(args.radius, args.nbr_vertices)

    # Vertices of the regular polygon with 'nbr_vertices' vertices
    vertices = get_rotated_points(initial_point, rotation_angles)

    # Save output to file
    outfile = args.output or '{0}v_{1}r_regular_polygon.yml'.format(args.nbr_vertices, args.radius)
    save_output(outfile, vertices)


if __name__ == '__main__':
    main(sys.argv)
