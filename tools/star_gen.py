#!/usr/bin/env python
#encoding: utf-8

import sys
import math
import argparse as ap
from utils import *


star_rotation_angles = map(math.radians, [0, 216, 72, 288, 144])


def parse_arguments():
    """
    Parses node arguments.
    """
    parser = ap.ArgumentParser()
    parser.add_argument('-r', '--radius', default=3.0, type=turtlesim_radius, help='radius of the smalles circle containing the wanted star.')
    parser.add_argument('-o', '--output', type=str,  help='Output file')
    return parser.parse_args()


def main(*args):
    """
    Main algorithm.
    """

    # Get arguments
    args = parse_arguments()

    # Intial point: (0, r)
    initial_point = (0, args.radius)

    # Vertices of a 5 points star
    vertices = get_rotated_points(initial_point, star_rotation_angles)

    # Save output to file
    outfile = args.output or '{0}r_star.yml'.format(args.radius)
    save_output(outfile, vertices)


if __name__ == '__main__':
    main(sys.argv)
