import '../imports/ui/connection_status.js';
import '../imports/ui/canvas.js';
import '../imports/ui/rosproxy.js';


Template.registerHelper('equals', function (a, b) {
      return a === b;
});