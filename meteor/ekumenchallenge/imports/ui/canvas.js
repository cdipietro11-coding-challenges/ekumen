import { Template } from 'meteor/templating';
import './templates.html';

/*
 * For further documentation of the objects and methods
 * used in this template, please refer to
 *   - http://threejs.org/docs/index.html#Manual/Introduction/Creating_a_scene
 *   - http://threejs.org/docs/index.html#Reference/Renderers/WebGLRenderer
 *   - http://threejs.org/docs/index.html#Reference/Materials/MeshBasicMaterial
 *   - http://threejs.org/docs/index.html#Reference/Extras.Geometries/CircleGeometry
 */


// var geometry, material, cube;
var camera;
var scene;
var renderer;
var circle;

function init() {
  /*
   * Initializes client's canvas.
   */

  var canvasWidth = 500;
  var canvasHeight = 500;

  // Camera
  camera = new THREE.PerspectiveCamera(30, canvasWidth/canvasHeight, 0.1, 1000);
  camera.position.set(5.544444561, 5.544444561, 20);

  // Scene
  scene = new THREE.Scene();
  scene.add(camera);
  
  // Renderer
  renderer = new THREE.WebGLRenderer();
  renderer.setClearColor(0x4556FF, 1);
  renderer.setSize(canvasWidth, canvasHeight);
  document.getElementById("canvas").appendChild(renderer.domElement);
}

function render() {
  /*
   * Renders current scene in client's canvas.
   */

  renderer.render(scene, camera);
}

Template.canvas.drawPosition = function(x, y, color) {
  /*
   * Draws a point in the client's canvas corresponding 
   * to a turtle current position. The point is drawn 
   * with the same color that the turtle has.
   */

  var material = new THREE.MeshBasicMaterial({color: color});
  var geometry = new THREE.CircleGeometry(0.05);
  var circle = new THREE.Mesh(geometry, material);
  circle.position.set(x, y, 0);
  scene.add( circle );
  render();
}


Template.canvas.onRendered(function() {
  init();
  render();
});
