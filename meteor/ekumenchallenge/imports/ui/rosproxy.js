import { Template } from 'meteor/templating';
import './templates.html';

/*
 * For further documentation of the objects and methods
 * used in this template, please refer to
 *  - http://wiki.ros.org/roslibjs#Tutorials
 *  - http://wiki.ros.org/roslibjs/Tutorials/BasicRosFunctionality
 */


var ros;
var turtles_topic;

function connect() {
  /*
   * Connects to the rosbridge WebSocket server
   * through a proxy in order to be able to 
   * call rosnodes services and subscribe to
   * their published topics.
   */

  // Create a rosbridge WebSocket server proxy.
  ros = new ROSLIB.Ros();

  // Set callbacks for connection
  ros.on('error', function(error) {
    Session.set('status', 'error');
    console.log("Unable to connect to ROSBridge: %s", error);
  });

  ros.on('connection', function() {
  console.log('Connection made!');
    Session.set('status', 'connected');
    console.log("Connected to ROSBridge.");
  });

  ros.on('close', function() {
    Session.set('status', 'closed');
    console.log('Connection to ROSBridge closed.');
  });

  // Connect to the rosbridge WebSocket server.
  ros.connect('ws://localhost:9090');
}

function subscribe_to_painter_turtles() {
  /*
   * Subscribes to '/painter_turtles/turtles' topic
   * and ejecutes a callback that draws in the canvas
   * the turtle position informed on each received message.
   */

  // Create a Topic object with details of the topic's name and message type.
  turtles_topic = new ROSLIB.Topic({
    ros : ros,
    name : '/painter_turtles',
    messageType : 'ekumenchallenge/TurtleData'

  });

  // Subscribe to topic and call the argument function
  // as a callback on each received message
  turtles_topic.subscribe(function (msg){

    console.log('Received message on %s: %s | %s | %s | %f | %f',
      turtles_topic.name,
      msg.name,
      msg.pen_color,
      msg.pen_on,
      msg.x,
      msg.y
    );

    // If turtle pen is on we render the new position
    if (msg.pen_on)
    {
      console.log('Rendering new position.');
      Template.canvas.drawPosition(msg.x, msg.y, msg.pen_color);
    }
  });
}


Template.rosproxy.onRendered(function() {
  connect();
  subscribe_to_painter_turtles();
});
