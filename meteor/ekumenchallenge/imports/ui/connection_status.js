import { Template } from 'meteor/templating';
import './templates.html';


Template.connection_status.onRendered(function() {
	Session.set('status', 'connecting');
});


Template.connection_status.helpers({
  status() {
    return Session.get('status');
  },
});