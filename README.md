# Ekumen Challenge
---

## 1. Introduction
This repository contains a [ROS](http://www.ros.org/)/[Meteor](https://www.meteor.com/) 
stack based application capable of making a group of turtles draw a 
given polygon in the [ROS turtlesim simulator](http://choosealicense.com/licenses/mit/). 
It also ensures that this figure is displayed on the screen in 
'real time' by a webapp developed in [Meteor](https://www.meteor.com/)
using [Three.js](http://threejs.org/) (a WebGL javascript wrapper).

This development was encouraged by the challenge proposed to me by the 
[Ekumen](http://www.ekumenlabs.com/) Company in the context of a job
interview.

![One turtle drawing an octagon.](screenshots/octagon.png)
![Many trurtles drawing together a star](screenshots/star.png)

![Figures drawn by turtles also can be displayed in a browser.](screenshots/webapp.png)


## 2. System Requirements
The application was developed and tested in a 
[VirtualBox](https://www.virtualbox.org/) virtual machine with the 
following hardware specs:
- [Ubuntu 14.04](http://releases.ubuntu.com/14.04/)
- 4 GB RAM
- 4 CPU cores


## 3. Dependencies
In order to be able to run the application you will need to 
have installed the following frameworks and dependencies:
- [ROS Indigo](http://wiki.ros.org/indigo)
- [Meteor 1.3](https://www.meteor.com/)
- [Three.js 0.76](http://threejs.org/)
- [python 2.7.6](https://www.python.org/)
- [python-argparse](https://pypi.python.org/pypi/argparse)
- [python-yaml](http://pyyaml.org/)
- [python-numpy](http://www.numpy.org/)
- [python-colour](https://pypi.python.org/pypi/colour/)


## 4.Installation
The installation process can be splited in two parts. The first part
consists in install all the application dependencies such as frameworks
and individual libraries. The second one consist in the installation
of the two application components: the ros package and the web-app.

### 4.1 Install dependencies

#### 4.1.1 Install necessary Python modules
As a first step, make sure you system package index and packages are 
up to date.
```bash
$ sudo apt-get update
$ sudo ap-get -y upgrade
```

Then, install the Python package manager PIP at its latest version.
```bash
# Install PIP package manager
$ sudo apt-get install python-pip python-dev build-essential 

# Update PIP and virtualenv
$ sudo pip install --upgrade pip virtualenv 
```

After that, you are ready to go and install the necessary Python
modules.
```bash
# Install python modules
$ sudo pip install argparse yaml numpy colour
```

#### 4.1.2 Install ROS Indigo framework
First of all, we need to add ROS repository into our Ubuntu 
sources list
```bash
# Add ROS repository to /etc/sources.list
$ sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

# Add ROS repository authentication key
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net --recv
```

Once we have done that, we need to update the system package index
so we can later find ros packages and install them
```bash
# Update Ubuntu package index
$ sudo apt-get update
```

Now we can install the full version of ROS, the ROSBridge package
and the ROS command-line tool
```bash
# Install ROS Deskotp-Full
$ sudo apt-get install ros-indigo-desktop-full

# Install ROSBridge Server
$ sudo apt-get install ros-indigo-rosbridge-server

# Install rosinstall command-line tool
$ sudo apt-get install python-rosinstall
```

Finally, we have to setup our ros workspace and environment 
```bash
# Initialize ROSdep
$ sudo rosdep init
$ rosdep update

## Setup the environment
$ echo "source /opt/ros/indigo/setup.bash" >> ~/.bashrc
$ source ~/.bashrc

# Create Catkin workspace
$ mkdir -p ~/catkin_ws/src
$ cd ~/catkin_ws/src
$ catkin_init_workspace
$ cd ~/catkin_ws/
$ catkin_make
$ echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc
$ source ~/.bashrc
```

#### 4.1.2 Install Meteor framework
To install Meteor you just have to run an unique command
as follows
```bash
# Install Meteor
$ curl https://install.meteor.com/ | sh
```

Now that you have completed all the former steps, you are ready to 
move on and install the application.

## 4.2 Install Application Components
The application is composed by two modules:
+ A ROS package containing a ros node with all the logic to control 
the turtles and make them draw a given polygon.
+ A Meteor webapp that communicates with ROS through the ROSBridge
server in order to get information about the turtles position and
draw their paths on the screen.

Following, you will find the instructions to install each of these
components.

### 4.2.1 Ros package
Create a new directory to host the ROS package
```bash
$ mkdir -p /opt/ekumenchallenge/rospkg
```

Copy ROS package into the new directory
```bash
$ cp -r <path_to_repo>/ekumenchallenge/rospkg/ekumenchallenge /opt/ekumenchallenge/rospkg
```

Link the ROS package to your catkin workspace and re-build it
```bash
$ ln -s /opt/ekumenchallenge/rospkg/ekumenchallenge ~/catkin_ws/src/
$ cd ~/catkin_ws
$ catkin_make
```

Copy the tools that generates input files with figures for rosnode to draw 
into the project system directory
```bash
$ cp -r <path_to_repo>/ekumenchallenge/tools /opt/ekumenchallenge/
```

Finally, give execution privileges to the all the executable Python scripts
```bash
$ chmod +x /opt/ekumenchallenge/rospkg/ekumenchallenge/scripts/painter_turtles
$ chmod +x /opt/ekumenchallenge/tools/regular_polygon_gen.py
$ chmod +x /opt/ekumenchallenge/tools/star_gen.py
```

### 4.2.2 Meteor Webapp
Create a new directory to host the Meteor app
```bash
$ mkdir -p /opt/ekumenchallenge/meteor
```

Create a new Meteor project inside the new directory
```bash
$ cd /opt/ekumenchallenge/meteor/
$ meteor create ekumenchallenge
$ cd ./ekumenchallenge
$ meteor npm install
$ meteor add session
```

Copy webapp sources into the new Meteor project
```bash
$ copy -r <path_to_repo>/ekumenchallenge/meteor/ekumenchallenge/client /opt/ekumenchallenge/meteor/ekumenchallenge
$ copy -r <path_to_repo>/ekumenchallenge/meteor/ekumenchallenge/server /opt/ekumenchallenge/meteor/ekumenchallenge
$ copy -r <path_to_repo>/ekumenchallenge/meteor/ekumenchallenge/imports /opt/ekumenchallenge/meteor/ekumenchallenge
```
Now you are all set up to go and try the application.


# 5. Use
You can run the rosnode in standalone mode or make it work 
together with the Meteor webapp. The necessary steps to achieve
each of those configurations are detailed below.

## 5.1 Lauching ROS node


### 5.1.1 Lauching ROS node manually
Open a new terminal, source your catkin workspace environment
and run *roscore*
```bash
# Terminal 1
$ source ~/catkin_ws/devel/setup.bash
$ roscore
```

Again, open Open a new terminal, source your catkin workspace 
environment and run *turtlesim_node*
```bash
# Terminal 2
$ source ~/catkin_ws/devel/setup.bash
$ rosrun turtlesim turtlesim_node
```

At last, open Open a new terminal, source your catkin workspace 
environment and run *painter_turtles* node
```bash
# Terminal 3
$ source ~/catkin_ws/devel/setup.bash
$ rosrun ekumenchallenge painter_turtles <path_to_input_file>
```

After that, you shall see that the turtles are spawned and they
start to draw the figure indicated in the input file.

Once all the turtles have returned to the center of the map
you will have to stop the ros *painter_turtles* node by
pressing 'Ctrl + C'.

You can re-run the *painter_turtles* node as many times as you 
want varying the input file. When you decide to stop, just close
all of the three terminals.


### 5.1.2 Lauching ROS node automatically
Alternatively, you can avoid the manual steps and launch all the
necessary ros nodes using a *.launch* file. 

For this, open a new terminal and execute the following commands
```bash
$ source ~/catkin_ws/devel/setup.bash
$ roslaunch ekumenchallenge painter_turtles.launch infile:=<path_to_input_file>
```

Notice that in this case, the input file must be preceded with the
*infile:=* prefix.

After that, you shall see that the turtles are spawned and they
start to draw the figure indicated in the input file.

Once all the turtles have returned to the center of the map
you will have to stop the roslaunch script by pressing 
'Ctrl + C'.

You can re-run the roslaunch script as many times as you 
want varying the input file. When you decide to stop, just 
close the terminal.


## 5.2 Running the Meteor webapp

The webapp has to communicate with the ROS core node in order to 
be able to subscribe to certain topics and get information about
the turtles pose, color, etc.

To accomplish this communication, a ROSBridge server needs to be
launched. To do so, open a new terminal, source your catkin workspace 
environment and call ROSBridge server *.launch* script
```bash
# Terminal 1
$ source ~/catkin_ws/devel/setup.bash
$ roslaunch rosbridge_server rosbridge_websocket.launch
```

Then, in a new terminal issue the following commands to start the webapp
```bash
# Terminal 2
$ cd /opt/ekumenchallenge/meteor/ekumenchallenge
$ meteor
```

This will launch the Meteor webapp server. To check if server is up,
open a browser and go to
```
http://localhost:3000
```

A webpage as follows shoud appear:

![](screenshots/fresh_webapp.png)

If you reached this point, then the webapp was properly launched.
Now, you just need to launch a *painter_turtles* node to start seeing
the figure in the browser. Please refere to section 5.1 for this matter.


# 6. Version
Current version is 0.1.0. Please take into account that this still is an alpha version.


# 7. Authors
This proyect was entirely developed by Carlos A. Di Pietro.


# 8. Licence
This software is distributed under the [MIT License](http://choosealicense.com/licenses/mit/).

# 9. Final Comments
As it was already pointed out this project is currently under an early stage 
of alpha version.Therefore there are many aspects to correct and improve in 
order to obtain a more reliable, robust and usable application.

Some of these improvements could be:

1. Fix *painter_turtles node* to make it close properly when pressing 'Ctrl+ C'.
2. Add support for multiple figures per input file.
3. Make the application more configurable. Remove from each application module
the hard-coded values such as ports, colors, sizes, coordinates, etc., and 
put them in individual config files.
4. Give to the Meteor client a connection retrial logic for the cases where
it can't connect to the ROSBrige server.
5. Put Meteor server to handle the communication with ROS core through ROSBridge, 
and make it trasmit the obtained information to all his clients.