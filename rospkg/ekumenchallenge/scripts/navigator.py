# coding: utf-8

import rospy
import numpy as np

from utils.utils import normalize_angle


class Navigator(object):
    """
    Navigator abstract class.
    """

    @classmethod
    def compute_rotation(cls, pose, goal):
        """
        Computes the gap angle to shift from current pose
        to a new one where the turtle is facing the goal. 
        """

        # Compute new direction vector between current 
        # position and the goal
        new_direction_vector = goal - np.array([pose.x, pose.y])

        # Compute angle between new direction vector 
        # and the x-axis
        new_dir_orientation = np.arctan2([new_direction_vector[1]], [new_direction_vector[0]])[0]

        # Compute the gap between current orientation
        # and the new direction vector orientation
        gap = normalize_angle(new_dir_orientation - pose.theta)
        clockwise = False

        if gap > np.pi:
            gap = 2*np.pi - gap
            clockwise = True

        return gap, clockwise


    @classmethod
    def compute_distance(cls, pose, goal):
        """
        Computes the distance between turtle current position and 
        the goal.
        """
        
        new_direction_vector = goal - np.array([pose.x, pose.y])
        distance = np.linalg.norm(new_direction_vector)
        forward = True
        return distance, forward
