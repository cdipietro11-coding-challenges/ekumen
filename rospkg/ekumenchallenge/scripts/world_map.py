# coding: utf-8

import rospy

from collections import namedtuple

from turtlesim.srv import Spawn, Kill
from service_proxy import ServiceProxy


class WorldMap(object):
    """
    World Map abstract class.
    """
    
    __Point2D = namedtuple('Point', 'x y')
    
    # Map origin coordinates
    origin = __Point2D(0.0, 0.0)

    # Map center coordinates
    center = __Point2D(5.544444561, 5.544444561)

    _spawn_proxy = ServiceProxy('spawn', Spawn)
    _kill_proxy = ServiceProxy('kill', Kill)
    _clear_client = ServiceProxy('clear')
    _reset_proxy = ServiceProxy('reset')


    @classmethod
    def spawn(cls, name, pose):
        """
        Creates a new turtle in the map with the given
        name and displayed with the given pose.
        """

        cls._spawn_proxy.call(pose.x, pose.y, pose.theta, name)
        rospy.loginfo('world_map: Spawned %s at (%.2f,%.2f).', name, pose.x, pose.y)
        

    @classmethod
    def kill(cls, name):
        """
        Kills turtle named with the given name.
        """

        cls._kill_proxy.call(name)
        rospy.loginfo('world_map: Killed %s.', name)


    @classmethod
    def clear(cls):
        """
        Erases all lines drawn by turtles.
        """

        cls._clear_proxy.call()
        rospy.loginfo('world_map: Cleared map..')


    @classmethod
    def reset(cls):
        """
        Resets map to its defaults.
        """

        cls._reset_proxy.call()
        rospy.loginfo('world_map: Reseted turtlesim.')
