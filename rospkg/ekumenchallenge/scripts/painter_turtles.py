#!/usr/bin/env python
# coding: utf-8

import sys
import yaml
import rospy
import numpy as np

from colour import Color

from utils.utils import chunks
from ekumenchallenge.msg import TurtleData
from turtle import Turtle, INITIAL_POSE
from world_map import WorldMap
from navigator import Navigator



class PainterTurtles(object):
    """
    ROS node that puts turtles in turtlesim 
    to draw a given figure.
    """

    name = 'painter_turtles'

    def __init__(self, config_file, world_map=WorldMap):
        """
        Initialization method.
        """

        self._turtles = []
        self._vertices = []
        self._nbr_turtles = 0
        self._world_map = world_map
        
        # Initialize ros node 'painter_turtles'
        rospy.init_node(self.name, anonymous=False)

        # Load configuration from yaml file
        self.__load_config(config_file)
        
        # Bound number of necessary turtles 
        # to draw the figure
        self.__bound_nbr_turtles()

        # Create '/painter_turtles/turtles' topic publisher
        self._publisher = rospy.Publisher(self.name, TurtleData, queue_size=self._nbr_turtles*10)

        # Empty world map
        self._world_map.reset()
        self._world_map.kill('turtle1')


    def __load_config(self, config_file):
        """
        Loads number of turtles and the list of vertices
        from a yaml file.
        """
        try:
            content = yaml.load(config_file)
            self._vertices = content['vertices']
            self._nbr_turtles = int(content['nbr_turtles'])

        except yaml.YAMLError as e:
            rospy.loginfo("%s: %s", self.name, e)
            sys.exit(1)


    def __bound_nbr_turtles(self):
        """
        Limit number of turtles down to the number of
        vertices in the given figure
        """
        
        nbr_vertices = len(self._vertices)
        if self._nbr_turtles > nbr_vertices:
            rospy.loginfo(
                "%s: %d turtles are too many for the given figure. Launching %d instead.",
                self.name,
                self._nbr_turtles,
                nbr_vertices
            )
            self._nbr_turtles = nbr_vertices


    def __launch_turtles(self):
        """
        Spawns a set of turtles to draw the figure defined by the
        points corresponding to each coordinate.
        """
        
        # Map vertices arround turtles initial positions
        center_vertex = lambda v: np.array(v) + np.array([INITIAL_POSE.x, INITIAL_POSE.y])
        self._vertices = map(center_vertex, self._vertices)

        # Split vertices into proper chunks so each turtle
        # gets to draw an equal amount of edges
        vertices_chunks = chunks(self._vertices, self._nbr_turtles)
        for i in range(self._nbr_turtles):
            next_chunk_first_vertex = vertices_chunks[(i+1)%self._nbr_turtles][0]
            world_center = np.array(list(self._world_map.center))
            vertices_chunks[i].append(next_chunk_first_vertex)
            vertices_chunks[i].append(world_center)

        # Create the turtles and get assign them a chunk of
        # segments to draw 
        for chunk in vertices_chunks:

            # Set new turtle name
            turtle_name = 'turtle{0}'.format(len(self._turtles)+1)
            
            # Create new turtle
            turtle = Turtle(turtle_name, INITIAL_POSE, WorldMap, Navigator)
            self._turtles.append(turtle)

            # Change tutle's pen color
            turtle.pen_color = Color(pick_for=turtle)

            # Put turtle to visit vertices
            turtle.set_goals(chunk)

        # Put turtles to draw
        for turtle in self._turtles:
            turtle.start()


    def __publish_on_topic(self):
        """
        Publishes messages with turtles data in the
        '/painter_turtles/turtles' topic
        """

        def gen_turtle_data(turtle):
            """
            Given a turtle generates a message
            with data about it.
            """

            current_pos = turtle.get_position()
            return TurtleData(turtle.name, turtle.pen_color.get_hex_l(),
                turtle.pen_on, current_pos[0], current_pos[1])

        def first_msg(msg):
            """
            Returns true if the message is None. 
            False otherwise.
            """

            return msg is None

        def changed(msg, last_msg):
            """
            Returns True if position in msg differs from
            the one in last_msg. False otherwise.
            """

            return last_msg.x != msg.x or last_msg.y != msg.y

        # Map containing each turtle last sent message        
        turtles_last_msgs = {}
        
        # Publish message about turtle's data 
        # until node is shutdown
        rate = rospy.Rate(60) # 60hz
        while not rospy.is_shutdown():

            # Generate a message for each turtle in the map
            for msg in map(gen_turtle_data, self._turtles):

                # If message possition differs from the previous one
                # and the turtle pen is on, publish it
                last_msg = turtles_last_msgs.get(msg.name)
                if first_msg(last_msg) or (changed(msg, last_msg) and msg.pen_on):
                    self._publisher.publish(msg)
                    rospy.logdebug("Publishing %s updated data: %s", msg.name, msg)
                    turtles_last_msgs[msg.name] = msg
            
            # Sleep to guarantee the choosed rate
            rate.sleep()


    def run(self):
        """
        Main algorithm.
        """

        try:
            # Launch turtles to draw the figure 
            # formed by connecting the vertices
            self.__launch_turtles()

            # Public into turtles names, positions and colors
            # in '/painter_turtles/turtles' topic
            self.__publish_on_topic()

        except rospy.ROSInterruptException:
            pass

        finally:
            # Send signal to all turtles to stop drawing
            # and wait for them to finish.
            for turtle in self._turtles:
                turtle.stop()
                turtle.join()
