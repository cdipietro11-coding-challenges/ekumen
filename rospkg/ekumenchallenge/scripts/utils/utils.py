# coding: utf-8

import numpy as np


def normalize_angle(angle):
    """
    Express angle between 0 and 2*PI.
    """

    return angle % (2*np.pi)


def chunks(l, n):
    """
    Creates n equal (+/-1) sized chunks from l.
    """

    res = []
    last = 0.0
    while last < len(l):
        avg = len(l) / float(n)
        chunk = l[int(last):int(last + avg)]
        res.append(chunk)
        last += avg
    return res


# def chunks(l, n):
#     """
#     Yield n successive chunks from l.
#     """
#     avg = len(l) / float(n)
#     last = 0.0
#     while last < len(l):
#         yield l[int(last):int(last + avg)]
#         last += avg
