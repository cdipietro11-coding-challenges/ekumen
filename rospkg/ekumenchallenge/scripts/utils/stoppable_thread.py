# coding: utf-8

import threading

class StoppableThread(threading.Thread):
    """
    Thread class with a stop() method. 
    The thread itself has to check regularly 
    for the stopped() condition.
    """

    def __init__(self):
    	"""
    	"""

        super(StoppableThread, self).__init__()
        self._stop = threading.Event()
        self._stop.clear()


    def stop(self):
    	"""
    	Stops the thread.
    	"""
        
        self._stop.set()


    def resume(self):
    	"""
    	Resumes a stopped thread.
    	"""
    	
    	if self.stopped():
    		self._stop.clear()
    		self.run


    def stopped(self):
    	"""
    	Retuns True if thread is stopped. False otherwise.
    	"""

        return self._stop.is_set()