# coding: utf-8

import rospy

from std_srvs.srv import Empty
from turtlesim.msg import Pose


class ServiceProxy(object):
    """
    A proxy for calling rosnodes services.
    """

    def __init__(self, svc_name, svc_type=Empty):
        """
        Initialization method.
        """

        # Service name
        self._svc_name = svc_name

        # Service message type
        self._svc_type = svc_type

        # Create a proxy for the service
        rospy.wait_for_service(self._svc_name)
        self._svc_proxy = rospy.ServiceProxy(self._svc_name, self._svc_type)


    def call(self, *args):
        """
        Calls service through internal proxy.
        """

        try:
            return self._svc_proxy(*args)
        except rospy.ServiceException as e:
            rospy.logerr("Service call failed: %s", e)
