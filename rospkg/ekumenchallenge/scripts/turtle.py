# coding: utf-8

import rospy
import time
import Queue
import threading
import numpy as np

from geometry_msgs.msg import Twist
from turtlesim.msg import Pose
from turtlesim.srv import SetPen, SetPenRequest

from utils.stoppable_thread import StoppableThread
from utils.utils import normalize_angle
from world_map import WorldMap
from service_proxy import ServiceProxy


INITIAL_POSE = Pose(
    x = WorldMap.center.x,
    y = WorldMap.center.y,
    theta = 0.0,
    linear_velocity = 3.0,
    angular_velocity = 1
)


class Turtle(StoppableThread):
    """
    Turtle class.
    """

    _rotation_epsilon = 0.017453292519943295    # 1 degree in radians
    _displacement_epsilon = 0.05


    def __init__(self, name, pose, world_map, navigator):
        """
        Initialization method.
        """

        super(Turtle, self).__init__()

        # Public Attributes
        self.name = name                                # Turtles's name
        self.pen_color = None                           # Turtle's pen color
        self.pen_on = True                             # Turtle's pen status
        self.linear_velocity = pose.linear_velocity     # Turtle linear velocity
        self.angular_velocity = pose.angular_velocity   # Turtle angular velocity
        self.pose = pose                                # Turtle's pose
        
        # Private Attributes
        self._pose_lock = threading.RLock()             # Turtle's pose mutex
        self._goals = Queue.Queue()                     # Turtle's target coordinates
        self._visited_goals = 0                         # Turtle's visited goals counter
        self._pose_subscription = None                  # Turtle's pose subscription
        self._cmd_vel_publisher = None                  # Turtle's velocity command publisher
        self._world_map = world_map                     # World map object colaborator
        self._navigator = navigator                     # Navigator object colaborator

        # Dislplay turtle in world map
        self._world_map.spawn(self.name, pose)
        rospy.loginfo('%s: Hello. I am new in the neighbourhood.', self.name)
     
        # Turn turtle's pen off
        self.toogle_pen()

        # Start /<name>/pose subscription
        pose_topic = '/{0}/pose'.format(self.name)
        self._pose_subscription = rospy.Subscriber(pose_topic, Pose, self.__update_position)
        rospy.logdebug('%s: Subscribed to topic %s.', pose_topic)        
 
        # Create /<name>/cmd_vel publisher
        velocity_topic = '/{0}/cmd_vel'.format(self.name)
        self._cmd_vel_publisher = rospy.Publisher(velocity_topic, Twist, queue_size=10)
        rospy.logdebug('%s: Registered as publisher at topic %s.', velocity_topic)        


    def set_goals(self, goals):
        """
        Sets a new list of goals for the turtle.
        """

        rospy.loginfo('%s: Received new figure to draw.', self.name)
        for goal in goals:
            self._goals.put(goal)


    def toogle_pen(self):
        """
        Toogles turtle's pen on and off.
        """

        # Toogle pen
        self.pen_on = not self.pen_on

        # Call turtle service to apply changes
        proxy = ServiceProxy('/{0}/set_pen'.format(self.name), SetPen)
        msg = SetPenRequest()
        
        if not self.pen_on:
            msg.off = 1
            rospy.loginfo('%s: Turned my pen off.', self.name)
        
        else:
            msg.r = self.pen_color.red*255
            msg.g = self.pen_color.green*255
            msg.b = self.pen_color.blue*255
            msg.width = 3
            rospy.loginfo('%s: Turned my pen on.', self.name)
        
        proxy.call(msg)            


    def get_position(self):
        """
        Returns a 2D array with the turtle current position.
        """
        with self._pose_lock:
            position = np.array([self.pose.x, self.pose.y])
        return position


    def get_orientation(self):
        """
        Returns the angle of turtle current orientation.
        """
        with self._pose_lock:
            angle = self.pose.theta
        return angle


    def run(self):
        """
        Makes turtle visit each of its goals.
        """

        while not self.stopped():
            try:
                goal = self._goals.get(block=True, timeout=1)
                
                if self._visited_goals == 1:
                    # As we arrived to the first point of the figure,
                    # we set the pen on.
                    self.toogle_pen()
                    
                elif self._goals.empty():
                    # As we finished to draw the figure and we are
                    # going back to the center, we set the pen on.
                    self.toogle_pen()

                rospy.loginfo('%s: Going to point (%.2f, %.2f)', self.name, *goal)
                self.rotate_to_goal(goal)
                time.sleep(0.1) # sleep to let rotation finish
                self.move_to_goal(goal)
                self._visited_goals += 1

            except Queue.Empty:
                rospy.logdebug('%s: No goals yet to visit.', self.name)
                continue
            
        rospy.loginfo('%s: Finished my work here. Bye!.', self.name)


    def rotate_to_goal(self, goal):
        """
        Rotates the turtle until if faces the goal.
        """

        with self._pose_lock:
            pose = self.pose

        angle, clockwise = self._navigator.compute_rotation(pose, goal)
        rospy.loginfo('%s: Rotating %s degrees %s', self.name, 
            np.degrees(angle), 'clockwise' if clockwise else 'counter-clockwise')
        self.rotate(angle, clockwise)


    def move_to_goal(self, goal):
        """
        Moves the turtle until the goal is reached.
        @pre: Assumes the turtle is already facing the goal.
        """

        with self._pose_lock:
            pose = self.pose

        distance, forward = self._navigator.compute_distance(pose, goal)
        rospy.loginfo('%s: Moving %s %s direction', self.name, distance, 
            'forward' if forward else 'backward')
        self.move(distance, forward)


    def rotate(self, angle, clockwise=False):
        """
        Rotates the turtle the specified angle either
        clockwise or counter-clockwise.
        """

        # Create rotation velocity command
        speed = abs(self.angular_velocity)
        vel_cmd = Twist()
        vel_cmd.angular.z = -speed if clockwise else speed

        # Start rotation
        current_angle = self.get_orientation()
        desired_angle = (-1 if clockwise else 1) * angle + current_angle
        orientation_gap = 2*self._rotation_epsilon

        rate = rospy.Rate(60) # 60hz
        while orientation_gap > self._rotation_epsilon:

            # Publish velocity command 
            self._cmd_vel_publisher.publish(vel_cmd)

            # Get current orientation data
            current_angle = self.get_orientation()

            # Compute new orientation gap
            orientation_gap = normalize_angle(desired_angle - current_angle)

            rospy.logdebug("desired_angle: %s", np.degrees(desired_angle))
            rospy.logdebug("current_angle: %s", np.degrees(current_angle))
            rospy.logdebug("orientation_gap: %s", np.degrees(orientation_gap))

            # Block process to fulfil the rate
            rate.sleep()
        
        # Stop rotation
        vel_cmd.angular.z = 0.0
        self._cmd_vel_publisher.publish(vel_cmd)


    def move(self, distance, forward=True):
        """
        Moves the turtle the specified distance either 
        forward or backward.
        """
        
        # Create displacement velocity command
        speed = abs(self.linear_velocity)
        vel_cmd = Twist()
        vel_cmd.linear.x = speed if forward else -speed

        # Start displacement
        initial_position = self.get_position()
        distance_gap = 2 * self._displacement_epsilon

        rate = rospy.Rate(60) # 60hz

        while distance_gap > self._displacement_epsilon:

            # Publish velocity command 
            self._cmd_vel_publisher.publish(vel_cmd)
            
            # Compute new current distance
            current_distance = np.linalg.norm(self.get_position() - initial_position)

            # Compute new distance gap
            distance_gap = abs(distance - current_distance)

            rospy.logdebug("current_distance: %f", current_distance)
            rospy.logdebug("distance: %f", distance)
            rospy.logdebug("distance_gap: %f", distance_gap)
            
            # Block process to fulfil the rate
            rate.sleep()

        # Stop displacement
        vel_cmd.linear.x = 0.0
        self._cmd_vel_publisher.publish(vel_cmd)    


    def __update_position(self, pose_msg):
        """
        Updates the turtle's pose by listening the messages 
        at the /<turtle.name>/pose topic.
        """

        with self._pose_lock:
            self.pose = pose_msg
        rospy.logdebug('%s: Pose updated to:\n%s', self.name, self.pose)
